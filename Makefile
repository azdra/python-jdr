build:
	@docker-compose -f docker-compose.yml build

start:
	@docker-compose up

stop:
	@docker-compose down

enter:
	@docker-compose exec python_api sh

install:
	@docker-compose run --rm python_api pip install -r requirements.txt
	@docker-compose run --rm web npm install