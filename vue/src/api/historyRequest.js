import {makeRequest} from "@/api/makeRequest";

export const getHistoryList = async () => {
    return await makeRequest("/api/history", "GET")
}