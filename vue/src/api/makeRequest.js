import axios from "axios";
import {API_URL} from "@/config/config";

export const makeRequest = async (url, method, body) => {
  const response = await axios({
    url: url,
    method,
    data: body,
    baseURL: API_URL
  })
  return response.data
}

