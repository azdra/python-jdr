from django.db import models

from dialogue.models import Dialogue


# Create your models here.
class DialogueChoice(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    dialogue = models.ForeignKey(Dialogue, on_delete=models.CASCADE, related_name='choices')
    next_dialogue = models.ForeignKey(Dialogue, on_delete=models.CASCADE, related_name='next_dialogue')

    def __str__(self):
        return f'DIALOGUE-CHOICES {self.id} - {self.content}'
