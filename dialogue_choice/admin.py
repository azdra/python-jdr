from django.contrib import admin

from dialogue_choice.models import DialogueChoice


# Register your models here.
class DialogueChoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'content', 'created_at', 'updated_at', 'dialogue', 'next_dialogue')


admin.site.register(DialogueChoice, DialogueChoiceAdmin)
