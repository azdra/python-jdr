from rest_framework import serializers

from dialogue_choice.models import DialogueChoice


class DialogueChoiceSerialize(serializers.ModelSerializer):
    class Meta:
        model = DialogueChoice
        fields = ['id', 'content']