from django.contrib import admin
from .models import Dialogue


# Register your models here.
class DialogueAdmin(admin.ModelAdmin):
    list_display = ('id', 'content', 'type', 'history', 'parent')


admin.site.register(Dialogue, DialogueAdmin)
