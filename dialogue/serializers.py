from rest_framework import serializers

from dialogue.models import Dialogue
from dialogue_choice.serializers import DialogueChoiceSerialize


class DialogueSerializer(serializers.ModelSerializer):
    choices = DialogueChoiceSerialize(many=True, read_only=True)

    class Meta:
        model = Dialogue
        fields = ['id', 'choices']
