from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from dialogue.models import Dialogue
from dialogue.serializers import DialogueSerializer


class DialogueViews:
    @api_view(['GET'])
    def get_list_of_dialogues(self: Request):
        queryset = Dialogue.objects.prefetch_related('choices').all()
        serializer_class = DialogueSerializer(queryset, many=True)
        return Response(serializer_class.data, status=status.HTTP_200_OK)

    @api_view(['GET'])
    def get_dialogue_by_id(self: Request, pk: int):
        queryset = Dialogue.objects.prefetch_related('history').get(id=pk)
        serializer_class = DialogueSerializer(queryset)
        return Response(serializer_class.data, status=status.HTTP_200_OK)
