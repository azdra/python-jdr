from django.db import models

from history.models import History


# Create your models here.
class Dialogue(models.Model):
    DIALOGUE = 'dialogue'
    VICTORY = 'victory'
    DEFEAT = 'defeat'

    TYPE_CHOICES = (
        (DIALOGUE, 'dialogue'),
        (VICTORY, 'victory'),
        (DEFEAT, 'defeat'),
    )

    id = models.AutoField(primary_key=True)
    content = models.TextField()
    type = models.CharField(max_length=255, choices=TYPE_CHOICES, default=DIALOGUE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    history = models.ForeignKey(History, on_delete=models.CASCADE)
    parent = models.ForeignKey('Dialogue', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        unique_together = ['history', 'parent']
        ordering = ['created_at']

    def __str__(self):
        return f'DIALOGUE {self.id} - {self.content}'
