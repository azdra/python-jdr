from django.urls import path
from dialogue.views import DialogueViews

urlpatterns = [
    path('', DialogueViews.get_list_of_dialogues, name='get_list_of_dialogues'),
    path('<int:pk>/', DialogueViews.get_dialogue_by_id, name='get_dialogue_by_id'),
]
