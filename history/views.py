from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response
from history.models import History
from history.serializers import HistorySerialize


class HistoryView:
    @api_view(['GET'])
    def get_list_of_histories(self: Request):
        queryset = History.objects.all()
        serializer_class = HistorySerialize(queryset, many=True)
        return Response(serializer_class.data, status=status.HTTP_200_OK)
