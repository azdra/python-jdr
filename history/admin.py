from django.contrib import admin
from .models import History


class HistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')


admin.site.register(History, HistoryAdmin)

# Register your models here.
