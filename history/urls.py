from django.urls import path
from history.views import HistoryView

urlpatterns = [
    path('', HistoryView.get_list_of_histories, name='get_list_of_histories'),
]
