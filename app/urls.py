from django.contrib import admin
from django.urls import path, include

api_urlpatterns = [
    path('history/', include('history.urls')),
    path('dialogue/', include('dialogue.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(api_urlpatterns)),
]
